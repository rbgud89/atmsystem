package com.example.rbgud.atmsystem;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test(){
        setComma("1234523234231");
    }

    private String setComma(String str){
        String comStr="";
        ArrayList<String> subStrs = new ArrayList<>();

        for(int i=str.length(); i>0; i-=3){
            if(i-3 >= 0){
                subStrs.add(","+str.substring(i-3,i));
                System.out.println(i-3+","+i+": "+str.substring(i-3,i));
            } else {
                subStrs.add(str.substring(0,i));
                System.out.println("0,"+i+": "+str.substring(0,i));
            }

        }

        for(int i = subStrs.size()-1; i>=0; i--){
            comStr += subStrs.get(i);
        }

        if(comStr.charAt(0)==','){
            comStr =comStr.substring(1,comStr.length());
        }

        System.out.println(comStr);

        return comStr;
    }

    /*private String setComma(String str){
        String comStr="";
        int posCount = 0;
        for(int i =0; i<str.length(); i++){

            if(posCount % 3 == 0 ){
                comStr += str.charAt(i)+",";
            } else {
                comStr += str.charAt(i);
            }

            posCount++;
        }

        System.out.println(comStr);

        return comStr;
    }*/
}