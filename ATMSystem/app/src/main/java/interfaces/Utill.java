package interfaces;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Utill {

    public String getTime(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        String time= sdf.format(date);
        return time;
    }

    public ArrayList<Integer> mixPwNums(){
        ArrayList<Integer> btnNumbers = new ArrayList();
        Random random = new Random();
        int num;
        boolean overlap = false;

        int count=0;
        while(count < 12){

            overlap = false;
            num = random.nextInt(12);
            for(int i=0; i<count; i++){
                if(btnNumbers.get(i) == num){
                    overlap = true;
                    break;
                }
            }

            if(overlap==false){
                btnNumbers.add(count,num);
                count++;
            }
        }

        /*for (int i = 0; i < btnNumbers.size(); i++) {

            System.out.print(" "+btnNumbers.get(i));
        }
        System.out.println();*/

        return btnNumbers;
    }

    public String setComma(int value){
        return setComma(Integer.toString(value));
    }

    public String setComma(String str){
        String comStr="";
        ArrayList<String> subStrs = new ArrayList<>();

        for(int i=str.length(); i>0; i-=3){
            if(i-3 >= 0){
                subStrs.add(","+str.substring(i-3,i));
                System.out.println(i-3+","+i+": "+str.substring(i-3,i));
            } else {
                subStrs.add(str.substring(0,i));
                System.out.println("0,"+i+": "+str.substring(0,i));
            }

        }

        for(int i = subStrs.size()-1; i>=0; i--){
            comStr += subStrs.get(i);
        }

        if(comStr.charAt(0)==','){
            comStr =comStr.substring(1,comStr.length());
        }

        //System.out.println(comStr);

        return comStr;
    }


}
