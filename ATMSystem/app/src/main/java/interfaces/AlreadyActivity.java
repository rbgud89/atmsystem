package interfaces;

import android.content.Intent;
import android.os.Bundle;

public interface AlreadyActivity {

    void initResource();
    void unpackBundle();
    Intent settingIntent(Class<?> cls , Bundle bundle);
    Bundle makeBundle(Byte mode);

}
