package MyListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import java.util.ArrayList;

import DB.ATMDBManager;
import model.Deal;

public class DealAdapter extends BaseAdapter{

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Deal> deals;

    public DealAdapter(Context context, ArrayList<Deal> deals){
        this.mContext = context;
        this.deals = deals;
        this.mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return deals.size();
    }

    @Override
    public Object getItem(int position) {
        return deals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DealHolder dholder;

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_deal, null);
            dholder = new DealHolder();
            dholder.txv_num = (TextView) convertView.findViewById(R.id.txv_num);
            dholder.txv_card_number = (TextView) convertView.findViewById(R.id.txv_card_number);
            dholder.txv_deal_type = (TextView) convertView.findViewById(R.id.txv_deal_type);
            dholder.txv_amount = (TextView) convertView.findViewById(R.id.txv_amount);
            dholder.txv_balance = (TextView) convertView.findViewById(R.id.txv_balance);
            dholder.txv_content = (TextView) convertView.findViewById(R.id.txv_content);

            convertView.setTag(dholder);
        } else{
            dholder = (DealHolder) convertView.getTag();
        }

        dholder.txv_num.setText(Integer.toString(deals.get(position).getNum()));
        dholder.txv_card_number.setText(deals.get(position).getCard_num());
        dholder.txv_deal_type.setText(deals.get(position).getDeal_type());
        dholder.txv_amount.setText(Integer.toString(deals.get(position).getAmount()));
        dholder.txv_balance.setText(Integer.toString(deals.get(position).getBalance()));
        dholder.txv_content.setText(deals.get(position).getContent());

        return convertView;
    }

    public class DealHolder{
        public TextView txv_num;
        public TextView txv_card_number;
        public TextView txv_deal_type;
        public TextView txv_amount;
        public TextView txv_balance;
        public TextView txv_content;
    }
}
