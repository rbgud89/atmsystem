package MyListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import java.util.ArrayList;

import DB.ATMDBManager;
import model.Customer;

public class CustomerAdapter extends BaseAdapter{

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Customer> customers;
    private ATMDBManager atmdbManager;

    public CustomerAdapter(Context context, ArrayList<Customer> customers){
        this.mContext = context;
        this.customers = customers;
        this.mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        atmdbManager = new ATMDBManager(context);
    }

    @Override
    public int getCount() {
        return customers.size();
    }

    @Override
    public Object getItem(int position) {
        return customers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final CoustomerHolder cholder;

        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_customer, null);
            cholder = new CoustomerHolder();
            cholder.txv_card_id = (TextView) convertView.findViewById(R.id.txv_card_id);
            cholder.txv_name = (TextView) convertView.findViewById(R.id.txv_name);
            cholder.txv_card_number = (TextView) convertView.findViewById(R.id.txv_card_number);
            cholder.txv_card_pw = (TextView) convertView.findViewById(R.id.txv_card_pw);
            cholder.txv_asset = (TextView) convertView.findViewById(R.id.txv_balance);
            cholder.btn_delete = (Button) convertView.findViewById(R.id.btn_delete);

            convertView.setTag(cholder);
        } else{
            cholder = (CoustomerHolder) convertView.getTag();
        }

        cholder.txv_card_id.setText(customers.get(position).getCard_id());
        cholder.txv_name.setText(customers.get(position).getName());
        cholder.txv_card_number.setText(customers.get(position).getCard_number());
        cholder.txv_card_pw.setText(customers.get(position).getCard_pw());
        if(customers.get(position) == null){
            cholder.txv_asset.setText("0");
        } else{
            cholder.txv_asset.setText(Integer.toString(customers.get(position).getAsset()));
        }

        if(cholder.txv_name.getText().toString().equals("ADMIN")){
            cholder.btn_delete.setVisibility(View.INVISIBLE);
        }

        cholder.btn_delete.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String card_number = cholder.txv_card_number.getText().toString();
                atmdbManager.deleteCustomber(card_number);
                atmdbManager.deleteDeal(card_number);
                customers.remove(position);
                notifyDataSetChanged();

            }
        });

        return convertView;
    }

    public class CoustomerHolder{
        public TextView txv_card_id;
        public TextView txv_name;
        public TextView txv_card_number;
        public TextView txv_card_pw;
        public TextView txv_asset;
        public Button btn_delete;
    }
}
