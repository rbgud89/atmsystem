package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import java.util.ArrayList;

import DB.ATMDBManager;
import MyListView.LatestDealAdapter;
import interfaces.AlreadyActivity;
import model.Customer;
import model.Deal;
import interfaces.Utill;

public class ReceiptActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener {

    private ATMSInfo atmsys;
    private ATMDBManager atmdbManager;
    private Utill utill;

    private Button btn_receipt_print;
    private Button btn_receipt_cancel;
    private TextView txv_trans_info;
    private TextView txv_txv_info;
    private LinearLayout ll_latest;
    private ListView lv_latest;

    private String endMsg;
    private boolean isReceipt;

    //unpack bundle data
    private byte mode;
    private int transMoney;
    private String card_id;
    private Customer recipient;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

        txv_trans_info.setText(printReceipt(mode));

    }

    private String printReceipt(byte mode){
        String str_info = null;
        //Customer customer = cm.getCustomer(card_id);
        Deal deal = null;
        if(mode != ATMSInfo.MODE_VIEW){
            deal= atmdbManager.selectLastDeal(card_number);
            txv_trans_info.setVisibility(View.VISIBLE);
            ll_latest.setVisibility(View.GONE);
        } else {
            txv_trans_info.setVisibility(View.GONE);
            ll_latest.setVisibility(View.VISIBLE);
        }

        switch (mode){
            case ATMSInfo.MODE_INPUT:
                str_info =  "[거래 내역]\n"+
                        "입금 금액 : "+ utill.setComma(deal.getAmount())+"\n"+
                        "임급 후 잔고 : "+utill.setComma(deal.getBalance())+"\n";

                break;
            case ATMSInfo.MODE_OUTPUT:
                str_info =  "[거래 내역]\n"+
                        "출금 금액 : "+utill.setComma(deal.getAmount())+"\n"+
                        "출금 후 잔고 : "+utill.setComma(deal.getBalance())+"\n";
                break;
            case ATMSInfo.MODE_VIEW:
                txv_trans_info.setTextSize(15);

                ArrayList<Deal> deals = atmdbManager.selectLimitDeals(card_number);
                txv_txv_info.setText("현재 잔고: "+deals.get(0).getBalance());
                LatestDealAdapter latestDealAdapter = new LatestDealAdapter(this, deals);
                lv_latest.setAdapter(latestDealAdapter);
                break;
            case ATMSInfo.MODE_TRANSFER:
                Deal reciDeal= atmdbManager.selectLastDeal(recipient.getCard_number());
                str_info =  "이체 금액 : "+ utill.setComma(deal.getAmount()) +
                            "\n이체 받는분 : "+recipient.getCard_number()+
                            "\n이체 후 잔고 : "+utill.setComma(reciDeal.getBalance()) +
                            "\n이체 보내는분 : "+deal.getCard_num() +
                            "\n이체 후 잔고 : "+utill.setComma(deal.getBalance()) ;
                break;
        }

        return str_info;
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        transMoney = bundle.getInt(atmsys.KEY_TRANS_MONEY);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        recipient = bundle.getParcelable(atmsys.KEY_RECIPIENT);
        card_number =bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("ReceiptActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id: "+card_id);
        atmsys.atmlog("거래금액: "+ transMoney);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        atmdbManager = new ATMDBManager(this);
        utill = new Utill();

        isReceipt = false;

        btn_receipt_print = (Button) findViewById(R.id.btn_receipt_print);
        btn_receipt_cancel = (Button) findViewById(R.id.btn_receipt_cancel);
        txv_trans_info = (TextView) findViewById(R.id.txv_trans_info);
        txv_txv_info = (TextView) findViewById(R.id.txv_info);
        ll_latest = (LinearLayout) findViewById(R.id.ll_latest);
        lv_latest = (ListView) findViewById(R.id.lv_latest);

        btn_receipt_print.setOnClickListener(this);
        btn_receipt_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()){
            case R.id.btn_receipt_print:
                isReceipt = true;
                endMsg ="명세표와 카드를 받아주세요.";
                intent = settingIntent(ReturnCardActivity.class, makeBundle(mode));
                startActivity(intent);
                break;
            case R.id.btn_receipt_cancel:
                isReceipt = false;
                endMsg ="카드를 받아주세요.";
                intent = settingIntent(ReturnCardActivity.class, makeBundle(mode));
                startActivity(intent);
                break;
        }
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();

        bundle.putByte(atmsys.KEY_MODE, mode);
        bundle.putString(atmsys.KEY_END_MESSAGE, endMsg);
        bundle.putBoolean(atmsys.KEY_ISRECEIPT, isReceipt);
        bundle.putInt(atmsys.KEY_TRANS_MONEY, transMoney);
        bundle.putString(atmsys.KEY_CARDID, card_id);
        bundle.putString(atmsys.KEY_CARDNUM, card_number);
        return bundle;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }
}
