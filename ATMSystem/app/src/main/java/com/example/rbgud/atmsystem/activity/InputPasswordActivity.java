package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;
import com.example.rbgud.atmsystem.activity.Admin.AddCustomerActivity;

import java.util.ArrayList;

import DB.ATMDBManager;
import interfaces.AlreadyActivity;
import interfaces.Utill;

public class InputPasswordActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener {

    private ATMSInfo atmsys;
    private ATMDBManager atmdbManager;

    private Button btn_cancel;
    private Button btn_mixbtn;
    private Button btn_clean;
    private Button btn_delete;
    private Button[] buttons;

    private TextView tv_password;

    private Utill utill;
    private ArrayList<Integer> pwNums;

    private byte mode;
    private String card_id;
    private String card_number;

    private String pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_password);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();
    }

    @Override
    public void initResource(){

        pw = "";

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        atmdbManager = new ATMDBManager(this);

        btn_mixbtn = (Button) findViewById(R.id.btn_mixbtn);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_clean = (Button) findViewById(R.id.btn_clean);
        btn_delete = (Button) findViewById(R.id.btn_delete);

        btn_cancel.setOnClickListener(this);
        btn_mixbtn.setOnClickListener(this);
        btn_clean.setOnClickListener(this);
        btn_delete.setOnClickListener(this);

        tv_password = (TextView) findViewById(R.id.tv_password);

        buttons = new Button[12];

        buttons[0] = (Button) findViewById(R.id.btn_0);
        buttons[1] = (Button) findViewById(R.id.btn_1);
        buttons[2] = (Button) findViewById(R.id.btn_2);
        buttons[3] = (Button) findViewById(R.id.btn_3);
        buttons[4] = (Button) findViewById(R.id.btn_4);
        buttons[5] = (Button) findViewById(R.id.btn_5);
        buttons[6] = (Button) findViewById(R.id.btn_6);
        buttons[7] = (Button) findViewById(R.id.btn_7);
        buttons[8] = (Button) findViewById(R.id.btn_8);
        buttons[9] = (Button) findViewById(R.id.btn_9);
        buttons[10] = (Button) findViewById(R.id.btn_10);
        buttons[11] = (Button) findViewById(R.id.btn_11);

        for(Button btn : buttons){
            btn.setOnClickListener(this);
        }

        setPwNums();

    }

    private void setPwNums(){
        utill = new Utill();
        pwNums = utill.mixPwNums();

        for(int i=0; i < buttons.length; i++){

            if(pwNums.get(i) <10){
                buttons[i].setVisibility(View.VISIBLE);
                buttons[i].setText(Integer.toString(pwNums.get(i)));
            } else {
                buttons[i].setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id =bundle.getString(atmsys.KEY_CARDID);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("InputPasswordActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id: "+card_id);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_mixbtn:
                setPwNums();
                break;
            case R.id.btn_clean:
                pw = "";
                tv_password.setText("");
                break;
            case R.id.btn_delete:
                if(pw.length() > 0){
                    pw =pw.substring(0,pw.length()-1);
                    printPwStar(pw);
                }
                break;
            case R.id.btn_0: inputPw(0); checkPwAfterMoveActivity(); break;
            case R.id.btn_1: inputPw(1); checkPwAfterMoveActivity(); break;
            case R.id.btn_2: inputPw(2); checkPwAfterMoveActivity(); break;
            case R.id.btn_3: inputPw(3); checkPwAfterMoveActivity(); break;
            case R.id.btn_4: inputPw(4); checkPwAfterMoveActivity(); break;
            case R.id.btn_5: inputPw(5); checkPwAfterMoveActivity(); break;
            case R.id.btn_6: inputPw(6); checkPwAfterMoveActivity(); break;
            case R.id.btn_7: inputPw(7); checkPwAfterMoveActivity(); break;
            case R.id.btn_8: inputPw(8); checkPwAfterMoveActivity(); break;
            case R.id.btn_9: inputPw(9); checkPwAfterMoveActivity(); break;
            case R.id.btn_10: inputPw(10); checkPwAfterMoveActivity(); break;
            case R.id.btn_11: inputPw(11); checkPwAfterMoveActivity(); break;
        }
    }

    private void inputPw(int index){

        if(pw.length() < 4){
            pw = pw + buttons[index].getText();
            printPwStar(pw);
        }

    }

    private void printPwStar(String pw){
        String str="";
        for(int i =0; i<pw.length(); i++){
            str +="*";
        }

        tv_password.setText(str);
        atmsys.atmlog("pw: "+pw);
    }

    private void checkPwAfterMoveActivity(){
        if(pw.length() == 4){
            String correct_pw;

            correct_pw = atmdbManager.getCardPw(card_number);

            if(pw.equals(correct_pw)){
                atmsys.atmlog("맞는번호");
                moveActivity(mode);
            } else{
                Intent intent = settingIntent(MessageActivity.class, makeBundle(ATMSInfo.MODE_FINALMSG));
                startActivity(intent);
            }
        }
    }

    private void moveActivity(byte mode) {

        Intent intent = null;
        Bundle bundle = new Bundle();

        switch (mode){
            case ATMSInfo.MODE_OUTPUT :
            case ATMSInfo.MODE_TRANSFER :
                intent = settingIntent(InputMoneyActivity.class, makeBundle(mode));
                break;
            case ATMSInfo.MODE_VIEW :
                intent = settingIntent(ReceiptActivity.class, makeBundle(mode));
                break;
            case ATMSInfo.MODE_ADMIN :
                intent = settingIntent(AddCustomerActivity.class, makeBundle(mode));
                //미구현
                break;
            default:
                intent = settingIntent(MainActivity.class, bundle);
                break;
        }

        startActivity(intent);
        finish();
    }

    @Override
    public Bundle makeBundle(Byte mode){
        Bundle bundle = new Bundle();
        bundle.putByte(atmsys.KEY_MODE, mode);

        switch (mode){
            case ATMSInfo.MODE_FINALMSG:
                bundle.putString(atmsys.KEY_MESSAGE, "비밀번호가 맞지 않습니다.");
                break;
            case ATMSInfo.MODE_OUTPUT:
            case ATMSInfo.MODE_VIEW:
            case ATMSInfo.MODE_TRANSFER:
                bundle.putString(atmsys.KEY_CARDID, card_id);
                bundle.putString(atmsys.KEY_CARDNUM, card_number);
                break;
            case ATMSInfo.MODE_ADMIN :
                break;
        }

        return bundle;
    }

    @Override
    public Intent settingIntent(Class<?> cls , Bundle bundle){
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        if(mode != ATMSInfo.MODE_ADMIN){
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        }

        intent.putExtra("bundle", bundle);

        return intent;
    }

    @Override
    public void finish() {
        super.finish();
        if(atmdbManager !=null){
            atmdbManager.dbClose();
        }
        overridePendingTransition(0,0);
    }
}
