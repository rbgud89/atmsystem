package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import interfaces.AlreadyActivity;

public class ReturnCardActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener, TextToSpeech.OnInitListener {

    private final int SPEECH_INFOMATION = 01;
    private final int SPEECH_BYE = 02;

    private ATMSInfo atmsys;
    private TextToSpeech tts_infomation;

    private Button btn_getcard;
    private Button btn_getreceipt;
    private TextView txv_endmsg;

    private byte speechMode;

    //unpack bundle data
    private byte mode;
    private String endMsg;
    private boolean isReceipt;
    private int transMoney;
    private String card_id;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_card);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

        speechMode = SPEECH_INFOMATION;
        tts_infomation = new TextToSpeech(this, this);

        txv_endmsg.setText(endMsg);

        if(!isReceipt){
            btn_getreceipt.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        endMsg = bundle.getString(atmsys.KEY_END_MESSAGE);
        isReceipt = bundle.getBoolean(atmsys.KEY_ISRECEIPT);
        transMoney = bundle.getInt(atmsys.KEY_TRANS_MONEY);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("ReturnCardActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("isReceipt: "+ isReceipt);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        btn_getcard = (Button) findViewById(R.id.btn_getcard);
        btn_getreceipt = (Button) findViewById(R.id.btn_getreceipt);

        btn_getcard.setOnClickListener(this);
        btn_getreceipt.setOnClickListener(this);

        txv_endmsg = (TextView) findViewById(R.id.txv_endmsg);
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();

        bundle.putByte(atmsys.KEY_MODE, mode);
        bundle.putInt(atmsys.KEY_TRANS_MONEY, transMoney);
        bundle.putString(atmsys.KEY_CARDID, card_id);
        bundle.putString(atmsys.KEY_CARDNUM, card_number);
        return bundle;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_getcard:
                btn_getcard.setVisibility(View.INVISIBLE);
                checkFinish();
                break;
            case R.id.btn_getreceipt:
                btn_getreceipt.setVisibility(View.INVISIBLE);
                checkFinish();
                break;

        }
    }

    private void checkFinish(){
        if(btn_getcard.getVisibility() == View.INVISIBLE
                && btn_getreceipt.getVisibility() == View.INVISIBLE){

            switch (mode){
                case ATMSInfo.MODE_INPUT:
                case ATMSInfo.MODE_VIEW:
                case ATMSInfo.MODE_TRANSFER:
                    finishHandler();
                    break;
                case ATMSInfo.MODE_OUTPUT:
                    Intent intent = settingIntent(BillOutputActivity.class, makeBundle(mode));
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }

    private void finishHandler(){

        txv_endmsg.setText("이용해 주셔서 감사합니다.");

        speechMode = SPEECH_BYE;
        tts_infomation = new TextToSpeech(this, this);

        Handler finishHandler = new Handler();
        finishHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 3000);
    }

    @Override
    public void onInit(int i) {
        switch (speechMode){
            case SPEECH_INFOMATION:
                if(isReceipt){
                    tts_infomation.speak("명세표와 카드를 받아주세요.", TextToSpeech.QUEUE_FLUSH, null);
                } else {
                    tts_infomation.speak("카드를 받아주세요.", TextToSpeech.QUEUE_FLUSH, null);
                }
                break;
            case SPEECH_BYE:
                tts_infomation.speak("이용해주셔서 감사합니다.", TextToSpeech.QUEUE_FLUSH, null);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(tts_infomation != null){
            tts_infomation.shutdown();
        }
    }

    @Override
    public void finish() {
        super.finish();
        if(tts_infomation != null){
            tts_infomation.shutdown();
        }
        overridePendingTransition(0,0);

    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }


}
