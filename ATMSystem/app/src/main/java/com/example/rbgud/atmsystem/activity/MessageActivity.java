package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import interfaces.AlreadyActivity;

public class MessageActivity extends AppCompatActivity implements AlreadyActivity {

    private ATMSInfo atmsys;

    private TextView txv_msg;

    private Byte mode;
    private String inMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

        txv_msg.setText(inMsg);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        },3500);
    }

    @Override
    public void initResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        txv_msg = (TextView) findViewById(R.id.txv_msg);
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        inMsg = bundle.getString(atmsys.KEY_MESSAGE);

        atmsys.atmlog("MessageActivity");
        atmsys.modelog(mode);
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        return null;
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        return null;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }
}
