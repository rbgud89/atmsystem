package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import DB.ATMDBManager;
import DB.ContractDB;
import interfaces.AlreadyActivity;
import model.Customer;

public class InputAccountNumber extends AppCompatActivity implements AlreadyActivity, View.OnClickListener {

    private ATMSInfo atmsys;

    private Button btn_cancel;
    private Button btn_ok;
    private Button btn_1;
    private Button btn_2;
    private Button btn_3;
    private Button btn_4;
    private Button btn_5;
    private Button btn_6;
    private Button btn_7;
    private Button btn_8;
    private Button btn_9;
    private Button btn_0;
    private Button btn_delete;
    private Button btn_clean;
    private TextView txv_card_number;

    private String reci_card_number;
    private Customer recipient;

    //unpack bundle data
    private byte mode;
    private String card_id;
    private int transMoney;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_account_number);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id =bundle.getString(atmsys.KEY_CARDID);
        transMoney = bundle.getInt(atmsys.KEY_TRANS_MONEY);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("InputAccountNumber");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id: "+card_id);
        atmsys.atmlog("거래금액:: "+ transMoney);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        reci_card_number = "";
        recipient = null;

        txv_card_number = (TextView) findViewById(R.id.txv_card_number);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_1 = (Button) findViewById(R.id.btn_1);
        btn_2 = (Button) findViewById(R.id.btn_2);
        btn_3 = (Button) findViewById(R.id.btn_3);
        btn_4 = (Button) findViewById(R.id.btn_4);
        btn_5 = (Button) findViewById(R.id.btn_5);
        btn_6 = (Button) findViewById(R.id.btn_6);
        btn_7 = (Button) findViewById(R.id.btn_7);
        btn_8 = (Button) findViewById(R.id.btn_8);
        btn_9 = (Button) findViewById(R.id.btn_9);
        btn_0 = (Button) findViewById(R.id.btn_0);
        btn_delete = (Button) findViewById(R.id.btn_delete);
        btn_clean = (Button) findViewById(R.id.btn_clean);

        btn_cancel.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        btn_0.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_clean.setOnClickListener(this);
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();
        bundle.putByte(atmsys.KEY_MODE, mode);

        if(recipient != null ){
            bundle.putInt(atmsys.KEY_TRANS_MONEY, transMoney);
            bundle.putString(atmsys.KEY_CARDID, card_id);
            bundle.putParcelable(atmsys.KEY_RECIPIENT, recipient);
            bundle.putString(atmsys.KEY_CARDNUM, card_number);
        } else{
            if(checkMyNum ){
                bundle.putString(atmsys.KEY_MESSAGE, "스스로에게 이체 할 수는 없습니다.");
            }else {
                bundle.putString(atmsys.KEY_MESSAGE, "등록되지 않은 혹은 거래할수\n없는 카드번호입니다.");
            }

        }

        return bundle;
    }


    private boolean checkMyNum = false;
    @Override
    public void onClick(View view) {

        Intent intent;
        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_ok:
                if(card_number.equals(reci_card_number)){
                    checkMyNum = true;
                    intent = settingIntent(MessageActivity.class, makeBundle(mode) );
                    startActivity(intent);
                    break;
                }

                //이체할 계좌 정보 가져오기
                ATMDBManager atmdbManager = new ATMDBManager(this);
                recipient = atmdbManager.selectCustomer(ContractDB.SEL_WHERE_CARD_NUMBER, reci_card_number);

                if (recipient != null) {
                    intent = settingIntent(CheckBillActivity.class, makeBundle(mode));
                    startActivity(intent);
                } else {
                    intent = settingIntent(MessageActivity.class, makeBundle(mode) );
                    startActivity(intent);
                }
                break;

            case R.id.btn_1:
                addNumber("1");
                break;
            case R.id.btn_2:
                addNumber("2");
                break;
            case R.id.btn_3:
                addNumber("3");
                break;
            case R.id.btn_4:
                addNumber("4");
                break;
            case R.id.btn_5:
                addNumber("5");
                break;
            case R.id.btn_6:
                addNumber("6");
                break;
            case R.id.btn_7:
                addNumber("7");
                break;
            case R.id.btn_8:
                addNumber("8");
                break;
            case R.id.btn_9:
                addNumber("9");
                break;
            case R.id.btn_0:
                addNumber("0");
                break;
            case R.id.btn_delete:
                if(reci_card_number.length() > 0) {
                    reci_card_number = reci_card_number.substring(0, reci_card_number.length() - 1);
                    txv_card_number.setText(reci_card_number);
                }
                break;
            case R.id.btn_clean:
                reci_card_number ="";
                txv_card_number.setText(reci_card_number);
                break;
        }
    }

    private void addNumber(String addNum){

        //10자리 이상 입력불가
        if(reci_card_number.length() > 9){
            return;
        }

        reci_card_number = reci_card_number + addNum;

        //price 시작숫자가 0이면 0제거
        if(reci_card_number.charAt(0) =='0'){
            reci_card_number = reci_card_number.substring(1,2);
        }

        txv_card_number.setText(reci_card_number);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }

}
