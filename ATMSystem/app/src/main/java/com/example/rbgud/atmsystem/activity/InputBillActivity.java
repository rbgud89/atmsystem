package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import interfaces.AlreadyActivity;
import interfaces.Utill;

public class InputBillActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener, TextToSpeech.OnInitListener{

    private ATMSInfo atmsys;
    private Utill utill;
    private MediaPlayer mp;
    private TextToSpeech tts_infomation;

    private Button btn_cancel;
    private Button btn_close;
    private Button btn_bill_cancel;
    private Button btn_50000;
    private Button btn_10000;
    private Button btn_notbill;

    private ImageView img_manwon;
    private ImageView img_ohmanwon;

    private TextView txv_msg;
    private TextView txv_msg2;

    private int transMoney;
    private boolean resultCountBill; //지페 카운팅 결과
    private boolean notBill; //돈이 아닌것이 있는지

    //get bundle data
    private byte mode;
    private String card_id;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_bill);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("InputBillActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id:"+card_id);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        utill = new Utill();

        mp = new MediaPlayer();
        mp = MediaPlayer.create(this,R.raw.moneycount);
        mp.setLooping(false);

        transMoney = 0;
        resultCountBill = false;
        notBill = false;

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_close = (Button) findViewById(R.id.btn_close);
        btn_bill_cancel = (Button) findViewById(R.id.btn_bill_cancel);
        btn_50000 = (Button) findViewById(R.id.btn_50000);
        btn_10000 = (Button) findViewById(R.id.btn_10000);
        btn_notbill = (Button) findViewById(R.id.btn_notbill);
        txv_msg = (TextView) findViewById(R.id.txv_msg);
        txv_msg2 =(TextView) findViewById(R.id.txv_msg2) ;

        btn_cancel.setOnClickListener(this);
        btn_close.setOnClickListener(this);
        btn_bill_cancel.setOnClickListener(this);
        btn_50000.setOnClickListener(this);
        btn_10000.setOnClickListener(this);
        btn_notbill.setOnClickListener(this);

        img_manwon = (ImageView) findViewById(R.id.img_manwon);
        img_ohmanwon = (ImageView) findViewById(R.id.img_ohmanwon);

        img_manwon.setVisibility(View.INVISIBLE);
        img_ohmanwon.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_close://지폐구 닫기
                countingBill();
                break;
            case R.id.btn_bill_cancel: // 돈 다빼기
                transMoney = 0;
                notBill = false;
                visibleBillImg(transMoney);
                txv_msg.setText("지폐를 넣어주세요");
                break;
            case R.id.btn_50000:
                transMoney += 50000;
                visibleBillImg(transMoney);
                txv_msg.setText("넣은 금액 : "+ makeComma(transMoney)+"원");
                checkNotBill();
                break;
            case R.id.btn_10000:
                transMoney += 10000;
                visibleBillImg(transMoney);
                txv_msg.setText("넣은 금액 : "+ makeComma(transMoney)+"원");
                checkNotBill();
                break;
            case R.id.btn_notbill:
                notBill = true;
                visibleBillImg(transMoney);
                txv_msg.setText("넣은 금액 : "+ makeComma(transMoney)+"원");
                checkNotBill();
                break;
        }
    }

    private void visibleBillImg(int totalMoney){
        if (totalMoney >= 10000 && totalMoney < 50000){
            img_manwon.setVisibility(View.VISIBLE);
            img_ohmanwon.setVisibility(View.INVISIBLE);
        } else if (totalMoney >=50000 ){
            img_manwon.setVisibility(View.INVISIBLE);
            img_ohmanwon.setVisibility(View.VISIBLE);
        } else {
            img_manwon.setVisibility(View.INVISIBLE);
            img_ohmanwon.setVisibility(View.INVISIBLE);
        }
    }

    private String makeComma(int num){
        return utill.setComma(Integer.toString(num));
    }

    private void checkNotBill(){
        if(notBill){
            txv_msg.setText(txv_msg.getText()+"??");
        }
    }

    private void countingBill(){
        btn_10000.setVisibility(View.INVISIBLE);
        btn_50000.setVisibility(View.INVISIBLE);
        btn_bill_cancel.setVisibility(View.INVISIBLE);
        btn_cancel.setVisibility(View.INVISIBLE);
        btn_close.setVisibility(View.INVISIBLE);
        btn_notbill.setVisibility(View.INVISIBLE);

        img_ohmanwon.setVisibility(View.INVISIBLE);
        img_manwon.setVisibility(View.INVISIBLE);

        txv_msg2.setText("");

        txv_msg.setText("지폐를 세고 있습니다.\n잠시만 기달려 주세요.");

        tts_infomation = new TextToSpeech(this, this);

        mp.start();

        finishCountBill();
    }

    private void finishCountBill(){

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                if(transMoney >=10000 && notBill == false){
                    resultCountBill = true;
                } else{
                    resultCountBill = false;
                }

                moveActivity();
            }
        });
    }

    private void moveActivity(){
        Intent intent;
        if(resultCountBill){
            intent = settingIntent(CheckBillActivity.class, makeBundle(mode));
        } else{
            intent = settingIntent(MessageActivity.class, makeBundle(mode));
        }

        startActivity(intent);
        finish();
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();

        if(resultCountBill){
            bundle.putInt(atmsys.KEY_TRANS_MONEY, transMoney);
            bundle.putString(atmsys.KEY_CARDID, card_id);
            bundle.putString(atmsys.KEY_CARDNUM, card_number);
            //bundle.putBoolean(atmsys.KEY_RESULT_COUNT_BILL, resultCountBill);
        } else {
            if(notBill){
                bundle.putString(atmsys.KEY_MESSAGE, "지페가 아닌게 있습니다.");
            } else {
                bundle.putString(atmsys.KEY_MESSAGE, "지페가 없거나 지페를\n세는데 실패 했습니다.");
            }
        }

        bundle.putByte(atmsys.KEY_MODE, mode);
        return bundle;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(tts_infomation != null){
            tts_infomation.shutdown();
        }

    }

    @Override
    public void finish() {
        super.finish();
        if(tts_infomation != null){
            tts_infomation.shutdown();
        }
        overridePendingTransition(0,0);
    }

    @Override
    protected void onDestroy() {
        if(mp !=null){
            mp.release();
        }
        super.onDestroy();
    }

    @Override
    public void onInit(int i) {
        tts_infomation.speak("지폐를 세고 있습니다. 잠시만 기달려 주세요.", TextToSpeech.QUEUE_FLUSH, null);
    }
}
