package com.example.rbgud.atmsystem.activity.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rbgud.atmsystem.R;
import com.example.rbgud.atmsystem.activity.ATMSInfo;
import com.example.rbgud.atmsystem.activity.ReadNFCCardActivity;

import DB.ATMDBManager;
import DB.ContractDB;
import MyListView.CustomerAdapter;
import interfaces.AlreadyActivity;
import model.Customer;

import java.util.ArrayList;

public class AddCustomerActivity extends AppCompatActivity implements AlreadyActivity,View.OnClickListener {

    private ATMSInfo atmsys;
    private ATMDBManager atmdbManager;

    private ListView lv_customers;
    private Button btn_card_tagging;
    private Button btn_insert;
    private Button btn_deal_list;
    private EditText edit_card_id;
    private EditText edit_card_number;
    private EditText edit_name;
    private EditText edit_card_pw;
    private EditText edit_balance;

    private byte mode;

    private final int CALL_READ_NFC_ACTIVTY = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();
        doListView();
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);

        atmsys.atmlog("ReadNFCCardActivity");
        atmsys.modelog(mode);
    }

    @Override
    public void initResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        atmdbManager = new ATMDBManager(this);

        btn_card_tagging = (Button) findViewById(R.id.btn_card_tagging);
        btn_insert = (Button) findViewById(R.id.btn_insert);
        btn_deal_list = (Button) findViewById(R.id.btn_deal_list);

        edit_card_id = (EditText) findViewById(R.id.edit_card_id);
        edit_card_number = (EditText) findViewById(R.id.edit_card_number);
        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_card_pw = (EditText) findViewById(R.id.edit_card_pw);
        edit_balance = (EditText) findViewById(R.id.edit_balance);

        btn_card_tagging.setOnClickListener(this);
        btn_insert.setOnClickListener(this);
        btn_deal_list.setOnClickListener(this);

        lv_customers = (ListView) findViewById(R.id.lv_customers);
    }

    private void doListView(){

        ArrayList<Customer> customers = atmdbManager.selectAllCustomer();

        if(customers != null){
            CustomerAdapter customerAdapter = new CustomerAdapter(this, customers);
            lv_customers.setAdapter(customerAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_deal_list:
                intent = settingIntent(DealListActivity.class, makeBundle(mode));
                startActivity(intent);
                break;
            case R.id.btn_card_tagging:
                intent = settingIntent(ReadNFCCardActivity.class, makeBundle(mode));
                startActivityForResult(intent,CALL_READ_NFC_ACTIVTY);
                break;
            case R.id.btn_insert:
                String card_id, card_number, name, card_pw;
                int balance;
                card_id = edit_card_id.getText().toString();
                card_number = edit_card_number.getText().toString();
                name = edit_name.getText().toString();
                card_pw = edit_card_pw.getText().toString();
                if(edit_balance.getText().toString().equals("")){
                    balance=0;
                }else{
                    balance = Integer.parseInt(edit_balance.getText().toString());
                }

                if(card_id == null || card_id.equals("")){
                    Toast.makeText(this,"CARD_ID를 입력하세요.",Toast.LENGTH_SHORT).show();
                } else if(card_number == null || card_number.equals("")){
                    Toast.makeText(this, "CARD_NUMBER를 입력하세요.",Toast.LENGTH_SHORT).show();
                } else if(card_pw == null || card_pw.equals("")){
                    Toast.makeText(this, "CARD_PW를 입력하세요.",Toast.LENGTH_SHORT).show();
                } else if(card_pw.length() < 4){
                    Toast.makeText(this, "CARD_PW는 4자리 여야합니다.",Toast.LENGTH_SHORT).show();
                } else {
                    insertCustomer(card_id, card_number, name, card_pw, balance);
                }

                break;
        }
    }

    private void insertCustomer(String card_id, String card_number, String name, String card_pw, int balance)
    {
        if(atmdbManager.existCard(ContractDB.SEL_WHERE_CARD_ID, card_id)){
            Toast.makeText(this, "이미 있는 CARD_ID 입니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(atmdbManager.existCard(ContractDB.SEL_WHERE_CARD_NUMBER, card_number)){
            Toast.makeText(this, "이미 있는 CARD_NUMBER 입니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        atmdbManager.insertCustomer(card_id, card_number, name, card_pw);
        atmdbManager.insertDeal(card_number, ContractDB.CH_DEPO,balance, balance,"계좌생성");
        doListView();
        initEdit();
    }

    private void initEdit(){
        edit_card_id.setText("");
        edit_card_number.setText("");
        edit_name.setText("");
        edit_card_pw.setText("");
        edit_balance.setText("0");
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {

        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        intent.putExtra("bundle", bundle);

        return intent;
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();
        bundle.putByte(atmsys.KEY_MODE, mode);

        return bundle;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case CALL_READ_NFC_ACTIVTY:
                    edit_card_id.setText(data.getStringExtra(atmsys.KEY_CARDID));
                    break;
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
    }
}
