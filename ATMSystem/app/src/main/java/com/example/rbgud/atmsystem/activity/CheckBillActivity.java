package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import DB.ATMDBManager;
import DB.ContractDB;
import interfaces.AlreadyActivity;
import model.Customer;
import model.Deal;
import interfaces.Utill;

public class CheckBillActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener, TextToSpeech.OnInitListener {

    private final byte SPEECH_INFOMATION = 01;
    private final byte SPEECH_COUNTMONEY = 02;

    private ATMDBManager atmdbManager;
    private ATMSInfo atmsys;
    private Utill utill;
    private MediaPlayer mp;

    private TextToSpeech tts_infomation;
    private byte speechMode;

    private Button btn_cancel;
    private Button btn_ok;

    private TextView txv_transmoney;
    private TextView txv_msg;
    private TextView tv_title;

    private byte resultCm;

    //unpack bundle data
    private Byte mode;
    private String card_id;
    private String card_number;
    private int transMoney;
    private Customer recipient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_bill);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

        callTTS(SPEECH_INFOMATION);

        switch (mode){
            case ATMSInfo.MODE_INPUT:
                txv_transmoney.setText("입 금 액 : "+utill.setComma(Integer.toString(transMoney)));
                break;
            case ATMSInfo.MODE_OUTPUT:
                txv_transmoney.setText("출 금 액 : "+utill.setComma(Integer.toString(transMoney)));
                break;
            case ATMSInfo.MODE_TRANSFER:
                tv_title.setText("이 체 확 인");
                String info =   "수취인 : "+recipient.getName()+
                                "\n카드번호 : "+recipient.getCard_number();
                txv_msg.setText(info);
                txv_transmoney.setText("이 체 금 액 : "+utill.setComma(Integer.toString(transMoney)));
                break;
        }
    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);
        transMoney = bundle.getInt(atmsys.KEY_TRANS_MONEY);
        recipient = bundle.getParcelable(atmsys.KEY_RECIPIENT);

        atmsys.atmlog("CheckBillActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id :"+card_id);
        atmsys.atmlog("거래금액:: "+ transMoney);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        utill = new Utill();

        mp = new MediaPlayer();
        mp = MediaPlayer.create(this,R.raw.moneycount);
        mp.setLooping(false);

        atmdbManager = new ATMDBManager(this);

        resultCm = ATMSInfo.FAIL;

        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_ok = (Button) findViewById(R.id.btn_ok);

        btn_cancel.setOnClickListener(this);
        btn_ok.setOnClickListener(this);

        txv_transmoney = (TextView) findViewById(R.id.txv_transmoney);
        txv_msg = (TextView) findViewById(R.id.txv_msg);
        tv_title = (TextView) findViewById(R.id.tv_title);

    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();

        switch (resultCm){
            case ATMSInfo.SUSSES:
                bundle.putParcelable(atmsys.KEY_RECIPIENT, recipient);
                bundle.putInt(atmsys.KEY_TRANS_MONEY, transMoney);
                bundle.putString(atmsys.KEY_CARDID, card_id);
                bundle.putString(atmsys.KEY_CARDNUM, card_number);
                break;
            case ATMSInfo.FAIL_OUTPUTMONEY_ASSETLACK:
                bundle.putString(atmsys.KEY_MESSAGE, "출금또는 이체하려는 금액이 통장 잔액보다 많습니다.");
                break;
            case ATMSInfo.FAIL_UNKNOWID:
                bundle.putString(atmsys.KEY_MESSAGE, "거래처리에 실패했습니다. 죄송합니다.");
                break;
        }

        bundle.putByte(atmsys.KEY_MODE, mode);
        return bundle;
    }

    @Override
    public void onClick(View view) {

        Intent intent;

        switch (view.getId()){
            case R.id.btn_cancel:
                switch (mode){
                    case ATMSInfo.MODE_INPUT:
                        resultCm = ATMSInfo.SUSSES;
                        intent = settingIntent(BillOutputActivity.class, makeBundle(mode));
                        startActivity(intent);
                        finish();
                        break;
                    case ATMSInfo.MODE_OUTPUT:
                    case ATMSInfo.MODE_TRANSFER:
                        finish();
                }

                break;
            case R.id.btn_ok:
                okEvent();
                break;
        }
    }

    private void okEvent(){

        switch (mode){
            case ATMSInfo.MODE_INPUT:

                try{
                    Deal deal = atmdbManager.selectLastDeal(card_number);
                    int balance = deal.getBalance();

                    atmdbManager.insertDeal(card_number, ContractDB.CH_DEPO, balance+transMoney, transMoney, "입금");
                    dealAfterMoveActivty(ReceiptActivity.class, ATMSInfo.SUSSES);
                } catch (SQLException e){
                    dealAfterMoveActivty(MessageActivity.class, ATMSInfo.FAIL_UNKNOWID);
                }

                break;
            case ATMSInfo.MODE_OUTPUT:

                try{
                    Deal deal = atmdbManager.selectLastDeal(card_number);
                    int balance = deal.getBalance();

                    if(balance >= transMoney){
                        atmdbManager.insertDeal(card_number, ContractDB.CH_DRAW, balance-transMoney, transMoney, "출금");
                        resultCm = ATMSInfo.SUSSES;
                        countingBill();
                    } else {
                        dealAfterMoveActivty(MessageActivity.class, ATMSInfo.FAIL_OUTPUTMONEY_ASSETLACK);
                    }
                } catch(SQLException e) {
                    dealAfterMoveActivty(MessageActivity.class, ATMSInfo.FAIL_UNKNOWID);
                    return;
                }

                break;
            case ATMSInfo.MODE_TRANSFER:

                try{
                    Deal sender = atmdbManager.selectLastDeal(card_number);
                    Deal recider = atmdbManager.selectLastDeal(recipient.getCard_number());
                    int sender_balance = sender.getBalance();
                    int recider_blance = recider.getBalance();

                    if(sender_balance < transMoney){
                        dealAfterMoveActivty(MessageActivity.class, ATMSInfo.FAIL_OUTPUTMONEY_ASSETLACK);
                        return; // 잔고보다 이체금액이 크면 프로세스중지
                    }

                    atmdbManager.insertDeal(card_number, ContractDB.CH_TRAN, sender_balance-transMoney,
                            transMoney, recipient.getCard_number()+"에게 이체 함");


                    atmdbManager.insertDeal(recipient.getCard_number(), ContractDB.CH_TRAN, recider_blance+transMoney,
                            transMoney, card_number+"에게 이체 받음");
                    dealAfterMoveActivty(ReceiptActivity.class, ATMSInfo.SUSSES);

                } catch (SQLException e) {
                    e.printStackTrace();
                    dealAfterMoveActivty(MessageActivity.class, ATMSInfo.FAIL_UNKNOWID);
                    return;
                }
                break;

        }
    }

    private void dealAfterMoveActivty(Class<?> cls, byte resultcode){
        Intent intent;
        resultCm = resultcode;
        intent = settingIntent(cls, makeBundle(mode));
        startActivity(intent);
        finish();
    }

    private void countingBill(){
        btn_ok.setVisibility(View.INVISIBLE);
        btn_cancel.setVisibility(View.INVISIBLE);

        txv_msg.setText("지폐를 세고 있습니다.\n잠시만 기달려 주세요.");

        callTTS(SPEECH_COUNTMONEY);

        mp.start();

        finishCountBill();
    }

    private void finishCountBill(){

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Intent intent = settingIntent(ReceiptActivity.class, makeBundle(mode));
                startActivity(intent);
                finish();
            }
        });
    }

    private void callTTS(byte speechMode){
        this.speechMode = speechMode;
        if(tts_infomation !=null && tts_infomation.isSpeaking()){
            tts_infomation.stop();
        }
        tts_infomation = new TextToSpeech(this,this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        tts_infomation.shutdown();
    }

    @Override
    public void finish() {
        super.finish();
        tts_infomation.shutdown();
        overridePendingTransition(0,0);
    }

    @Override
    public void onInit(int i) {
        switch (speechMode){
            case SPEECH_INFOMATION:
                tts_infomation.speak("맞으시면 확인. 틀리시면 취소를 눌러 주세요.",TextToSpeech.QUEUE_FLUSH,null);
                break;
            case SPEECH_COUNTMONEY:
                tts_infomation.speak("지폐를 세고 있습니다. 잠시만 기달려 주세요.", TextToSpeech.QUEUE_FLUSH, null);
                break;

        }

    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }
}
