package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.example.rbgud.atmsystem.R;

import DB.ATMDBHelper;
import DB.ATMDBManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener{

    private ATMSInfo atmsys ;
    private TextToSpeech tts_infomation;
    private ATMDBManager atmdbManager;

    private ImageView img_muji_hello;
    private Button btn_input;
    private Button btn_output;
    private Button btn_transfer;
    private Button btn_view;
    private Button btn_admin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        atmsys = (ATMSInfo) getApplicationContext();

        initResource();
        //gifImageLoad();
    }

    private void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tts_infomation = new TextToSpeech(this, this);
        atmdbManager = new ATMDBManager(this);

        btn_input = (Button) findViewById(R.id.btn_input);
        btn_output = (Button) findViewById(R.id.btn_output);
        btn_transfer = (Button) findViewById(R.id.btn_transfer);
        btn_view = (Button) findViewById(R.id.btn_view);
        btn_admin = (Button) findViewById(R.id.btn_admin);

        btn_input.setOnClickListener(this);
        btn_output.setOnClickListener(this);
        btn_transfer.setOnClickListener(this);
        btn_view.setOnClickListener(this);
        btn_admin.setOnClickListener(this);
    }

    private void gifImageLoad(){
        img_muji_hello = (ImageView) findViewById(R.id.img_hello);

        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(img_muji_hello);
        Glide.with(this).load(R.drawable.muji_hello).into(gifImage);
    }

    @Override
    public void onInit(int i) {
        tts_infomation.speak("어서오십쇼. A.T.M.System입니다.", TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void finish() {
        super.finish();
        tts_infomation.shutdown();
        atmdbManager.dbClose();
    }

    @Override
    protected void onPause() {
        super.onPause();
        tts_infomation.shutdown();
    }

    @Override
    public void onClick(View view) {

        Intent intent;

        switch (view.getId()){
            case R.id.btn_input:
                Log.i("atmlog"," MainActivity 입금 클릭");

                intent = settingIntent(ReadNFCCardActivity.class, makeBundle(ATMSInfo.MODE_INPUT));
                startActivity(intent);
                break;

            case R.id.btn_output:
                Log.i("atmlog"," MainActivity 출금 클릭");

                intent = settingIntent(ReadNFCCardActivity.class, makeBundle(ATMSInfo.MODE_OUTPUT));
                startActivity(intent);
                break;

            case R.id.btn_transfer:
                Log.i("atmlog"," MainActivity 이체 클릭");

                intent = settingIntent(ReadNFCCardActivity.class, makeBundle(ATMSInfo.MODE_TRANSFER));
                startActivity(intent);
                break;

            case R.id.btn_view:
                Log.i("atmlog"," MainActivity 조회 클릭");

                intent = settingIntent(ReadNFCCardActivity.class, makeBundle(ATMSInfo.MODE_VIEW));
                startActivity(intent);
                break;

            case R.id.btn_admin:
                Log.i("atmlog"," MainActivity 관리자 클릭");

                intent = settingIntent(InputPasswordActivity.class, makeBundle(ATMSInfo.MODE_ADMIN));
                startActivity(intent);
                break;
        }
    }
    private Bundle makeBundle(Byte mode){
        Bundle bundle = new Bundle();

        bundle.putByte(atmsys.KEY_MODE, mode);
        if(mode == ATMSInfo.MODE_ADMIN){
            bundle.putString(atmsys.KEY_CARDID, "0");
            bundle.putString(atmsys.KEY_CARDNUM, "0");
        }
        return bundle;
    }

    private Intent settingIntent(Class<?> cls, Bundle bundle){
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY); //호출되는 activity는 activyty 스택에서 제거됨

        intent.putExtra("bundle", bundle);

        return intent;
    }



}
