package com.example.rbgud.atmsystem.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import DB.ATMDBManager;
import DB.ContractDB;
import interfaces.AlreadyActivity;
import model.Customer;

public class ReadNFCCardActivity extends AppCompatActivity implements AlreadyActivity, View.OnClickListener{

    private ATMSInfo atmsys;

    private static final String CHARS = "0123456789ABCDEF";
    private ATMDBManager atmdbManager;
    private NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;
    private MediaPlayer mp;

    private Button btn_cancel;
    private TextView txv_cardinfo;

    private byte mode;
    private String card_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_nfccard);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();
        initNFC();

    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);

        atmsys.atmlog("ReadNFCCardActivity");
        atmsys.modelog(mode);
    }

    @Override
    public void initResource() {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        atmdbManager = new ATMDBManager(this);

        mp = new MediaPlayer();
        mp = MediaPlayer.create(this,R.raw.beep1);
        mp.setLooping(false);

        txv_cardinfo = (TextView) findViewById(R.id.txv_cardinfo);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);

        btn_cancel.setOnClickListener(this);

    }

    private void initNFC() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent intent = new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        if(nfcAdapter != null){
            atmsys.atmlog("Read an NFC tag");
        } else {
            atmsys.atmlog("Not Read an NFC tag");
        }

        atmsys.atmlog("onCreate");
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
        atmsys.atmlog("onNewIntent");

        String card_id = null;
        card_id = readCardId(intent);

        switch (mode){
            case ATMSInfo.MODE_INPUT :
            case ATMSInfo.MODE_OUTPUT :
            case ATMSInfo.MODE_TRANSFER :
            case ATMSInfo.MODE_VIEW :
                checkCustomer(card_id);
                break;
            case ATMSInfo.MODE_ADMIN:
                txv_cardinfo.setText("카드가 인식되었습니다.\ncardID: "+card_id);
                Handler handler = new Handler();
                final String finalCard_id = card_id;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(atmsys.KEY_CARDID, finalCard_id);
                        setResult(RESULT_OK, resultIntent);
                        finish();
                    }
                }, 1000);


                break;
        }

    }

    private String readCardId(Intent intent){
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        String tagid = null;

        if (tag != null) {
            mp.start();

            byte[] tagId = tag.getId();

            tagid = toHexString(tagId);

            atmsys.atmlog("TagID: " + tagid);
        } else{
            tagid = null;
        }

        return tagid;
    }

    public static String toHexString(byte[] data) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < data.length; ++i) {

            sb.append(CHARS.charAt((data[i] >> 4) & 0x0F))
                    .append(CHARS.charAt(data[i] & 0x0F));

        }

        return sb.toString();

    }

    private void checkCustomer(String card_id) {
        if(card_id.equals("") || card_id == null){
            txv_cardinfo.setText("카드 인식에 실패하였습니다.");
        } else {
            txv_cardinfo.setText("카드가 인식되었습니다.\ncardID: "+card_id);

            if(atmdbManager.existCard(ContractDB.SEL_WHERE_CARD_ID, card_id)){

                this.card_id = card_id;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        moveActivity(mode);
                    }
                }, 1000);
            } else{
                txv_cardinfo.setText("ATMSystem에서는\n사용할 수 없는 유저입니다.");
            }
        }
    }

    private void moveActivity(byte mode) {

        Intent intent = null;
        Bundle bundle = new Bundle();

        switch (mode){
            case ATMSInfo.MODE_INPUT :
                intent = settingIntent(InputBillActivity.class, makeBundle(ATMSInfo.MODE_INPUT));
                break;
            case ATMSInfo.MODE_OUTPUT :
                intent = settingIntent(InputPasswordActivity.class, makeBundle(ATMSInfo.MODE_OUTPUT));
                break;
            case ATMSInfo.MODE_TRANSFER :
                intent = settingIntent(InputPasswordActivity.class, makeBundle(ATMSInfo.MODE_TRANSFER));
                break;
            case ATMSInfo.MODE_VIEW :
                intent = settingIntent(InputPasswordActivity.class, makeBundle(ATMSInfo.MODE_VIEW));
                break;
            default:
                intent = settingIntent(MainActivity.class, bundle);
                break;
        }

        startActivity(intent);
        finish();
    }

    @Override
    public Bundle makeBundle(Byte mode){
        Bundle bundle = new Bundle();
        bundle.putString(atmsys.KEY_CARDID, card_id);
        bundle.putByte(atmsys.KEY_MODE, mode);

        Customer customer = atmdbManager.selectCustomer(ContractDB.SEL_WHERE_CARD_ID, card_id);
        bundle.putString(atmsys.KEY_CARDNUM, customer.getCard_number());
        return bundle;
    }

    @Override
    public Intent settingIntent(Class<?> cls , Bundle bundle){
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }

    @Override
    protected void onResume() {
        super.onResume();

        atmsys.atmlog("onResume");
        if (nfcAdapter != null) {

            atmsys.atmlog("nfc ready");
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);

        } else {
            atmsys.atmlog("nfc not ready");
        }
    }

    @Override
    protected void onPause() {

        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }

        super.onPause();
    }


    @Override
    protected void onDestroy() {
        if(mp !=null){
            mp.release();
        }
        super.onDestroy();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }
}
