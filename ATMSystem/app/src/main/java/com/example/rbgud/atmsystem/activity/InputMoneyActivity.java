package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import interfaces.AlreadyActivity;
import interfaces.Utill;

public class InputMoneyActivity extends AppCompatActivity implements View.OnClickListener, AlreadyActivity{

    private ATMSInfo atmsys;
    private Utill utill;

    private Button btn_cancel;
    private Button btn_ok;
    private Button btn_1;
    private Button btn_2;
    private Button btn_3;
    private Button btn_4;
    private Button btn_5;
    private Button btn_6;
    private Button btn_7;
    private Button btn_8;
    private Button btn_9;
    private Button btn_0;
    private Button btn_delete;
    private Button btn_clean;
    private Button btn_man;
    private Button btn_chan;
    private TextView txv_price;
    private TextView txv_msg;

    private int trans_money;

    //unpack bundle data
    private byte mode;
    private String card_id;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_money);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle ;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("InputMoneyActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id: "+card_id);
    }

    @Override
    public void initResource(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        utill = new Utill();

        trans_money = 0;

        txv_price = (TextView) findViewById(R.id.txv_price);
        txv_msg = (TextView) findViewById(R.id.txv_msg);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_1 = (Button) findViewById(R.id.btn_1);
        btn_2 = (Button) findViewById(R.id.btn_2);
        btn_3 = (Button) findViewById(R.id.btn_3);
        btn_4 = (Button) findViewById(R.id.btn_4);
        btn_5 = (Button) findViewById(R.id.btn_5);
        btn_6 = (Button) findViewById(R.id.btn_6);
        btn_7 = (Button) findViewById(R.id.btn_7);
        btn_8 = (Button) findViewById(R.id.btn_8);
        btn_9 = (Button) findViewById(R.id.btn_9);
        btn_0 = (Button) findViewById(R.id.btn_0);
        btn_delete = (Button) findViewById(R.id.btn_delete);
        btn_clean = (Button) findViewById(R.id.btn_clean);
        btn_man = (Button) findViewById(R.id.btn_man);
        btn_chan = (Button) findViewById(R.id.btn_chan);

        btn_cancel.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        btn_0.setOnClickListener(this);
        btn_delete.setOnClickListener(this);
        btn_clean.setOnClickListener(this);
        btn_man.setOnClickListener(this);
        btn_chan.setOnClickListener(this);

        switch(mode){
            case ATMSInfo.MODE_OUTPUT:
                txv_msg.setText("출금하실 금액을 누르시고\n확인을 눌러 주십시오.\n(최대1억단위까지 가능)");
                break;
            case ATMSInfo.MODE_TRANSFER:
                txv_msg.setText("이체하실 금액을 누르시고\n확인을 눌러 주십시오.\n(최대1억단위까지 가능)");
                break;
        }
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        Bundle bundle = new Bundle();
        bundle.putByte(atmsys.KEY_MODE, mode);
        bundle.putString(atmsys.KEY_CARDID, card_id);
        bundle.putString(atmsys.KEY_CARDNUM, card_number);
        bundle.putInt(atmsys.KEY_TRANS_MONEY, trans_money);

        return bundle;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_ok:
                Intent intent;
                switch (mode){
                    case ATMSInfo.MODE_OUTPUT:
                        intent = settingIntent(CheckBillActivity.class, makeBundle(mode));
                        startActivity(intent);
                        break;
                    case ATMSInfo.MODE_TRANSFER:
                        intent = settingIntent(InputAccountNumber.class, makeBundle(mode));
                        startActivity(intent);
                }
                break;
            case R.id.btn_1:
                addNumber("1");
                break;
            case R.id.btn_2:
                addNumber("2");
                break;
            case R.id.btn_3:
                addNumber("3");
                break;
            case R.id.btn_4:
                addNumber("4");
                break;
            case R.id.btn_5:
                addNumber("5");
                break;
            case R.id.btn_6:
                addNumber("6");
                break;
            case R.id.btn_7:
                addNumber("7");
                break;
            case R.id.btn_8:
                addNumber("8");
                break;
            case R.id.btn_9:
                addNumber("9");
                break;
            case R.id.btn_0:
                addNumber("0");
                break;
            case R.id.btn_delete:
                if(trans_money > 0){
                    if(9>= trans_money){
                        trans_money = 0;
                        txv_price.setText(Integer.toString(trans_money));
                    } else{
                        String price = Integer.toString(trans_money);
                        price = price.substring(0, price.length()-1);
                        trans_money = Integer.parseInt(price);

                        price = utill.setComma(price);

                        txv_price.setText(price);
                    }
                }
                break;
            case R.id.btn_clean:
                trans_money = 0;
                txv_price.setText(Integer.toString(trans_money));
                break;
            case R.id.btn_man:
                addNumber("0000");
                break;
            case R.id.btn_chan:
                addNumber("000");
                break;
        }
    }

    private void addNumber(String addNum){
        String price  = Integer.toString(trans_money) + addNum;

        //10자리 이상 입력불가
        if(price.length() > 9){
            return;
        }

        //price 시작숫자가 0이면 0제거
        if(price.charAt(0) =='0'){
            price = price.substring(1,2);
        }

        //콤마가 없는 순수 정수price를 inputPrice에 저장
        trans_money = Integer.parseInt(price);

        price = utill.setComma(price);

        txv_price.setText(price);
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0,0);
    }

    @Override
    public Intent settingIntent(Class<?> cls , Bundle bundle){
        Intent intent = new Intent(getApplicationContext(), cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        intent.putExtra("bundle", bundle);

        return intent;
    }
}
