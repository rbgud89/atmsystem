package com.example.rbgud.atmsystem.activity.Admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import java.util.ArrayList;

import DB.ATMDBManager;
import MyListView.DealAdapter;
import model.Deal;

public class DealListActivity extends AppCompatActivity {

    private ListView lv_deal;

    private TextView txv_dealmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_list);

        initResource();
    }

    private void initResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ATMDBManager atmdbManager = new ATMDBManager(this);

        lv_deal = (ListView) findViewById(R.id.lv_deal);
        txv_dealmsg = (TextView) findViewById(R.id.txv_dealmsg);

        ArrayList<Deal> deals = atmdbManager.selectAllDeal();

        if(deals != null){
            DealAdapter dealAdapter = new DealAdapter(this, deals);
            lv_deal.setAdapter(dealAdapter);
            lv_deal.setVisibility(View.VISIBLE);
            txv_dealmsg.setVisibility(View.GONE);
        } else {
            lv_deal.setVisibility(View.GONE);
            txv_dealmsg.setVisibility(View.VISIBLE);
        }

    }
}
