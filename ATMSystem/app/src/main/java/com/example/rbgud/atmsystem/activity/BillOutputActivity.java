package com.example.rbgud.atmsystem.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.rbgud.atmsystem.R;

import java.util.HashMap;

import DB.ATMDBManager;
import DB.ContractDB;
import interfaces.AlreadyActivity;
import model.Deal;
import interfaces.Utill;

public class BillOutputActivity extends AppCompatActivity
        implements  AlreadyActivity, View.OnClickListener, TextToSpeech.OnInitListener  {

    private final byte SPEECH_INFOMATION = 01;
    private final byte SPEECH_BYE = 02;

    private ATMSInfo atmsys;
    private Utill utill;
    private MediaPlayer mp;
    private ATMDBManager atmdbManager;

    private TextToSpeech tts_infomation;
    private byte speechMode;

    private boolean isTrun = true;

    private Button btn_getbill;
    private TextView txv_transmoney;
    private TextView txv_msg;

    //unpack bundle data
    private Byte mode;
    private String card_id;
    private int transMoney;
    private String card_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_output);
        atmsys = (ATMSInfo) getApplicationContext();

        unpackBundle();
        initResource();

        txv_transmoney.setText(utill.setComma(Integer.toString(transMoney))+"원");

        ExceptionTimer();

    }

    @Override
    public void unpackBundle() {
        Intent intent = getIntent();
        Bundle bundle;
        bundle = intent.getBundleExtra("bundle");

        mode = bundle.getByte(atmsys.KEY_MODE);
        card_id = bundle.getString(atmsys.KEY_CARDID);
        transMoney = bundle.getInt(atmsys.KEY_TRANS_MONEY);
        card_number = bundle.getString(atmsys.KEY_CARDNUM);

        atmsys.atmlog("BillOutputActivity");
        atmsys.modelog(mode);
        atmsys.atmlog("현재 card_id :"+card_id);
        atmsys.atmlog("거래금액:: "+ transMoney);
    }

    @Override
    public void initResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        utill = new Utill();

        atmdbManager = new ATMDBManager(this);

        callTTS(SPEECH_INFOMATION);
        tts_infomation = new TextToSpeech(this, this);

        txv_transmoney = (TextView) findViewById(R.id.txv_transmoney);
        btn_getbill = (Button) findViewById(R.id.btn_getbill);
        txv_msg = (TextView) findViewById(R.id.txv_msg);

        btn_getbill.setOnClickListener(this);
    }

    private void ExceptionTimer(){

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int sc = 0;
                    Log.i("threadtest", "startThread");
                    while(isTrun){

                        Log.i("threadtest", "sc : "+ sc);
                        if(sc == 5){
                            Log.i("threadtest", "mpstart");
                            mpStart();
                        }

                        if(sc == 15){
                            Log.i("threadtest", "15sc---");
                            Deal deal = atmdbManager.selectLastDeal(card_number);
                            int balance = deal.getBalance();

                            atmdbManager.insertDeal(card_number, ContractDB.Ch_NOTGET,
                                    balance+transMoney, transMoney, "돈안가져가 재입금처리");
                            finish();
                            Log.i("threadtest", "NOTGET ---");
                            isTrun = false;
                        }
                        Thread.sleep(1000);
                        sc++;
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        thread.start();
    }

    private void mpStart(){
        mp = new MediaPlayer();
        mp = MediaPlayer.create(this, R.raw.beep1);
        mp.setLooping(true);
        mp.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_getbill:
                if(mp != null){
                    if(mp.isPlaying()){
                        mp.stop();
                    }
                }
                isTrun = false;
                callTTS(SPEECH_BYE);
                tts_infomation = new TextToSpeech(this, this);
                btn_getbill.setVisibility(View.INVISIBLE);
                txv_transmoney.setText("");
                break;
        }
    }

    private void callTTS(byte speechMode){
        this.speechMode = speechMode;
        if(tts_infomation !=null && tts_infomation.isSpeaking()){
            tts_infomation.stop();
        }
        tts_infomation = new TextToSpeech(this,this);
    }

    @Override
    public Intent settingIntent(Class<?> cls, Bundle bundle) {
        return null;
    }

    @Override
    public Bundle makeBundle(Byte mode) {
        return null;
    }

    @Override
    public void finish() {
        super.finish();
        exitProsess();
        overridePendingTransition(0,0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        exitProsess();
    }

    private void exitProsess(){
        if(tts_infomation.isSpeaking()){
            tts_infomation.stop();
        }
        tts_infomation.shutdown();
        
        if(mp !=null){
            mp.release();
        }
    }

    @Override
    public void onInit(int i) {
        switch (speechMode){
            case SPEECH_INFOMATION:
                tts_infomation.speak("지페를 가져가 주십시오.",TextToSpeech.QUEUE_FLUSH,null);
                break;
            case SPEECH_BYE:
                HashMap<String, String> params = new HashMap<>();
                // The utterance ID isn't actually used anywhere, the param is passed only to force
                // the utterance listener to be notified
                params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "kiwixLastMessage");
                tts_infomation.speak("이용해주셔서 감사합니다.", TextToSpeech.QUEUE_FLUSH, params);
                tts_infomation.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String s) {
                        Log.i("TEST","onStart");
                        txv_msg.setText("이용해주셔서 감사합니다.");

                    }

                    @Override
                    public void onDone(String s) {
                        Log.i("TEST","onDone");
                        finish();
                    }

                    @Override
                    public void onError(String s) {
                        Log.i("TEST","onError");
                    }
                });
                break;
        }

    }

}
