package com.example.rbgud.atmsystem.activity;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import model.Customer;


public class ATMSInfo extends Application{


    /* mode 설명
    input : 입금
    output : 출금
    transfer : 이체
    view : 조회
    finalMsg: 메세지화면
    */

    public static final byte MODE_INPUT = 001;
    public static final byte MODE_OUTPUT = 002;
    public static final byte MODE_TRANSFER = 003;
    public static final byte MODE_VIEW = 004;
    public static final byte MODE_ADMIN = 005;
    public static final byte MODE_FINALMSG = 006;

    /*에러 코드*/
    public static final byte SUSSES = 0;
    public static final byte FAIL_OUTPUTMONEY_ASSETLACK = 10;
    public static final byte FAIL_UNKNOWID = 11;
    public static final byte FAIL = 99;

    /* bundle key
        KEY_MODE : 모드 키
     */

    public final String KEY_MODE = "mode";
    public final String KEY_RECIPIENT = "recipient";
    public final String KEY_CARDID = "cardid";
    public final String KEY_CARDNUM = "cardnum";
    public final String KEY_MESSAGE = "message";
    public final String KEY_TRANS_MONEY ="inputmoney";
    public final String KEY_END_MESSAGE = "endmessage";
    public final String KEY_ISRECEIPT = "isreceipt";

    public final String TAG_ATMLOG = "atmlog";

    public void atmlog(String msg){
        Log.i(TAG_ATMLOG, msg);
    }

    public void modelog(byte mode){
        switch (mode){
            case MODE_INPUT:
                atmlog("현제 모드는 입금");
                break;
            case MODE_OUTPUT:
                atmlog("현제 모드는 출금");
                break;
            case MODE_TRANSFER:
                atmlog("현제 모드는 이체");
                break;
            case MODE_VIEW:
                atmlog("현제 모드는 조회");
                break;
        }
    }

}
