package model;

import android.os.Parcel;
import android.os.Parcelable;

public class Deal implements Parcelable {

    private int num;
    private String card_num;
    private String deal_type;
    private int balance;
    private int amount;
    private String content;
    private String regdate;

    public Deal(){}

    public Deal(int num, String card_num, String deal_type, int balance, int amount, String content, String regdate){
        this.num = num;
        this.card_num = card_num;
        this.deal_type = deal_type;
        this.balance = balance;
        this.amount = amount;
        this.content = content;
        this.regdate = regdate;
    }


    protected Deal(Parcel in) {
        num = in.readInt();
        card_num = in.readString();
        deal_type = in.readString();
        balance = in.readInt();
        amount = in.readInt();
        content = in.readString();
        regdate = in.readString();
    }

    public static final Creator<Deal> CREATOR = new Creator<Deal>() {
        @Override
        public Deal createFromParcel(Parcel in) {
            return new Deal(in);
        }

        @Override
        public Deal[] newArray(int size) {
            return new Deal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(num);
        dest.writeString(card_num);
        dest.writeString(deal_type);
        dest.writeInt(balance);
        dest.writeInt(amount);
        dest.writeString(content);
        dest.writeString(regdate);
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public String getDeal_type() {
        return deal_type;
    }

    public void setDeal_type(String deal_type) {
        this.deal_type = deal_type;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }
}
