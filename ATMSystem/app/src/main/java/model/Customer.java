package model;

import android.os.Parcel;
import android.os.Parcelable;

public class Customer implements Parcelable{

    private String card_id;
    private String card_number;
    private String name;
    private String card_pw;
    private int asset;
    private String regdate;

    public Customer(){ }

    protected Customer(Parcel in) {
        card_id = in.readString();
        card_number = in.readString();
        name = in.readString();
        card_pw = in.readString();
        asset = in.readInt();
        regdate = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    @Override
    public String toString() {
        return "Customer{" +
                "card_id='" + card_id + '\'' +
                ", card_number='" + card_number + '\'' +
                ", name='" + name + '\'' +
                ", card_pw='" + card_pw + '\'' +
                ", asset=" + asset +
                ", regdate='" + regdate + '\'' +
                '}';
    }

    public Customer(String card_id, String card_number, String name, String card_pw, int asset, String regdate){
        this.card_id = card_id;
        this.card_number = card_number;
        this.name = name;
        this.card_pw = card_pw;
        this.asset = asset;
        this.regdate  =regdate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(card_id);
        dest.writeString(card_number);
        dest.writeString(name);
        dest.writeString(card_pw);
        dest.writeInt(asset);
        dest.writeString(regdate);
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCard_pw() {
        return card_pw;
    }

    public void setCard_pw(String card_pw) {
        this.card_pw = card_pw;
    }

    public int getAsset() {
        return asset;
    }

    public void setAsset(int asset) {
        this.asset = asset;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }
}
