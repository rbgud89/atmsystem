package DB;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import model.Customer;
import model.Deal;
import interfaces.Utill;

import static DB.ContractDB.*;

public class ATMDBManager {

    private final String TAG = "ATMDB";

    private ATMDBHelper atmdbHelper;
    private SQLiteDatabase atmdb;
    private Utill utill;

    public ATMDBManager(Context context){
        atmdbHelper = new ATMDBHelper(context);

        if(!(atmdbHelper.getWritableDatabase().isOpen())){
            atmdbHelper.onOpen(atmdbHelper.getWritableDatabase());
        }
        atmdb = atmdbHelper.getWritableDatabase();

        utill = new Utill();
    }

    public ArrayList<Customer> selectAllCustomer(){
        String sql = ContractDB.SEL_ALL_CUSTOMER;
        Cursor cur = null;
        Customer customer = null;
        ArrayList<Customer> cts = null;

        Log.i(TAG, "selectAllCustomer > "+ sql);
        cur = atmdb.rawQuery(sql, null);

        if( cur.getCount() >0){
            cts = new ArrayList<>();
        }

        while (cur.moveToNext()){
            customer = new Customer();
            customer.setCard_id(cur.getString(0));
            customer.setCard_number(cur.getString(1));;
            customer.setName(cur.getString(2));
            customer.setCard_pw(cur.getString(3));
            customer.setRegdate(cur.getString(4));
            cts.add(customer);
        }

        return cts;
    }

    public Customer selectCustomer(String selctSql, String value){
        String sql = selctSql + "'" + value +"'";

        Cursor cur= null;
        Customer customer= null;

        Log.i(TAG, "selectCustomer() > "+sql);
        cur = atmdb.rawQuery(sql, null);
        if (cur.moveToNext()){
            customer = new Customer();
            customer.setCard_id(cur.getString(0));
            customer.setCard_number(cur.getString(1));;
            customer.setName(cur.getString(2));
            customer.setCard_pw(cur.getString(3));
            customer.setRegdate(cur.getString(4));
        }

        return  customer;
    }

    public ArrayList<Deal> selectAllDeal(){
        String sql = SEL_ALL_DEAL;
        Cursor cur = null;
        Deal deal = null;
        ArrayList<Deal> des = null;

        Log.i(TAG, "selectAllDeal > "+ sql);
        cur = atmdb.rawQuery(sql, null);

        if( cur.getCount() >0){
            des = new ArrayList<>();
        }

        while (cur.moveToNext()){
            deal = new Deal();
            deal.setNum(cur.getInt(0));
            deal.setCard_num(cur.getString(1));;
            deal.setDeal_type(cur.getString(2));
            deal.setBalance(cur.getInt(3));
            deal.setAmount(cur.getInt(4));
            deal.setContent(cur.getString(5));
            deal.setRegdate(cur.getString(6));
            des.add(deal);
        }

        return des;
    }

    public ArrayList<Deal> selectLimitDeals(String card_num){
        String sql =
                "SELECT " + COL_NUM +","+ COL_CARD_NUM +","+ COL_DEAL_TYPE+","+
                COL_BALANCE +","+ COL_AMOUNT +","+ COL_CONTENT + ","+ COL_REGDATE +
                " FROM " + TBL_DEALS +
                " WHERE " + COL_CARD_NUM + "=" + "'" + card_num+ "'" +
                " ORDER BY "+ COL_NUM+ " DESC" +
                " LIMIT 5";

        Cursor cur= null;
        ArrayList<Deal> deals = null;
        Deal de= null;


        Log.i(TAG, "selectLastDeal() > "+sql);
        cur = atmdb.rawQuery(sql, null);

        if(cur.getCount() > 0){
            deals = new ArrayList<>();
        }

        while (cur.moveToNext()){
            de = new Deal();
            de.setNum(cur.getInt(0));
            de.setCard_num(cur.getString(1));
            de.setDeal_type(cur.getString(2));
            de.setBalance(cur.getInt(3));
            de.setAmount(cur.getInt(4));
            de.setContent(cur.getString(5));
            de.setRegdate(cur.getString(6));
            deals.add(de);
        }

        return  deals;
    }

    public Deal selectLastDeal(String card_num){
        String sql = "SELECT " + COL_NUM +","+ COL_CARD_NUM +","+ COL_DEAL_TYPE+","+
                COL_BALANCE +","+ COL_AMOUNT +","+ COL_CONTENT +","+ COL_REGDATE +
                " FROM " + TBL_DEALS +
                " WHERE " +
                COL_NUM + "=" +
                "(SELECT MAX("+COL_NUM +") FROM "+ TBL_DEALS +
                " WHERE " +  COL_CARD_NUM + "='"+  card_num + "'" +
                ")";

        Cursor cur= null;
        Deal deal= null;

        Log.i(TAG, "selectLastDeal() > "+sql);
        cur = atmdb.rawQuery(sql, null);
        if (cur.moveToNext()){
            deal = new Deal();
            deal.setNum(cur.getInt(0));
            deal.setCard_num(cur.getString(1));
            deal.setDeal_type(cur.getString(2));
            deal.setBalance(cur.getInt(3));
            deal.setAmount(cur.getInt(4));
            deal.setContent(cur.getString(5));
            deal.setRegdate(cur.getString(6));
        }

        return  deal;
    }

    public boolean existCard(String selectSQL, String whereValue){
        String sql = selectSQL + "'"+whereValue+"'";
        Cursor cur = null;

        cur = atmdb.rawQuery(sql, null);
        if(cur.moveToNext()){
            return true;
        } else {
            return false;
        }
    }

    public String getCardPw(String card_number){
        Customer customer= null;
        customer = selectCustomer(ContractDB.SEL_WHERE_CARD_NUMBER,card_number);

        if(customer != null){
            return customer.getCard_pw();
        } else{
            return "";
        }
    }

    public void insertCustomer(String card_id, String card_number,
             String name, String card_pw){
        String sql = ContractDB.INS_CUSTOMER +
                "(" +
                "'"+card_id+"',"+
                "'"+card_number+"',"+
                "'"+name+"',"+
                "'"+card_pw+"',"+
                "'" + utill.getTime() + "'" +
                ")";

        atmdb.execSQL(sql);
    }

    public void deleteCustomber(String card_number){
        String sql = ContractDB.DEL_CUSTOMERS_WHERE_CARD_NUMBER +
                "'" + card_number + "'";

        atmdb.execSQL(sql);
    }

    public void insertDeal(String card_num , String deal_type,
                           int balance, int amount, String content)throws SQLException{


        String sql = ContractDB.INS_DEAL + "(" +
                "'" + card_num + "',"+
                "'" + deal_type + "'," +
                balance + "," +
                amount + "," +
                "'" + content +"',"+
                "'" + utill.getTime() + "'" +
                ")";
        atmdb.execSQL(sql);
    }

    public void deleteDeal(String card_number){
        String sql = ContractDB.DEL_DEALS_WHERE_CARD_NUM +
                "'" + card_number + "'";

        atmdb.execSQL(sql);
    }

    public void dbClose(){
        atmdbHelper.close();
    }


}
