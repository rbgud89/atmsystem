package DB;

public class ContractDB {

    private ContractDB(){

    }

    /*
    CREATE TABLE CUSTOMERS (
        CARD_ID TEXT NOT NULL UNIQUE,
        CARD_NUMBER TEXT PRIMARY KEY,
        NAME TEXT,
        CARD_PW TEXT
    );

    CREATE TABLE IF NOT EXISTS DEALS
    (
        NUM INTEGER PRIMARY KEY AUTOINCREMENT,
        CARD_NUM TEXT NOT NULL,
        DEAL_TYPE TEXT NOT NULL,
        BALANCE INTEGER NOT NULL,
        AMOUNT INTEGER NOT NULL,
        CONTENT TEXT NOT NULL,
        FOREIGN KEY(CARD_NUM) REFERENCES CUSTOMERS(CARD_NUMBER)
    );
*/

    public static final String TBL_DEALS = "DEALS";
    public static final String COL_CARD_NUM = "CARD_NUM";
    public static final String COL_DEAL_TYPE = "DEAL_TYPE";
    public static final String COL_BALANCE = "BALANCE";
    public static final String COL_AMOUNT = "AMOUNT";
    public static final String COL_CONTENT = "CONTENT";
    public static final String COL_REGDATE = "REGDATE";

    public static final String CH_DEPO = "DEPO";
    public static final String CH_DRAW = "DRAW";
    public static final String CH_TRAN = "TRAN";
    public static final String Ch_NOTGET = "NOTGET"; //돈을 안가져감

    public static final String TBL_CUSTOMERS = "CUSTOMERS";
    public static final String COL_NUM = "NUM";
    public static final String COL_CARD_ID = "CARD_ID";
    public static final String COL_CARD_NUMBER = "CARD_NUMBER";
    public static final String COL_NAME = "NAME";
    public static final String COL_CARD_PW = "CARD_PW";


    public static final String CREATE_TBL_DEALS =
        "CREATE TABLE IF NOT EXISTS " + TBL_DEALS + " " +
        "(" +
        COL_NUM +           " INTEGER PRIMARY KEY AUTOINCREMENT" + "," +
        COL_CARD_NUM +      " TEXT NOT NULL" + "," +
        COL_DEAL_TYPE +     " TEXT NOT NULL" + "," +
        COL_BALANCE +       " INTEGER NOT NULL" + "," +
        COL_AMOUNT +        " INTEGER NOT NULL" + "," +
        COL_CONTENT +       " TEXT NOT NULL" + "," +
        COL_REGDATE +       " TEXT NOT NULL" + "," +
        "FOREIGN KEY(" + COL_CARD_NUM + ")" +
        " REFERENCES " + TBL_CUSTOMERS +"(" + COL_CARD_NUMBER + ")" +
        ")";

    public static final String CREATE_TBL_CUSTOMERS =
        "CREATE TABLE IF NOT EXISTS " + TBL_CUSTOMERS +" " +
        "(" +
        COL_CARD_ID +       " TEXT NOT NULL UNIQUE" + ", " +
        COL_CARD_NUMBER +   " TEXT PRIMARY KEY"+ ", " +
        COL_NAME +          " TEXT" +               ", " +
        COL_CARD_PW +       " TEXT NOT NULL" + ", " +
        COL_REGDATE +       " TEXT NOT NULL" +
        ")";

    public static final String INS_ADMIN =
        "INSERT INTO " + TBL_CUSTOMERS +
        "(" + COL_CARD_ID +","+ COL_CARD_NUMBER +","+ COL_NAME+","+
              COL_CARD_PW +","+ COL_REGDATE + ")" +
        "VALUES" +
        "(" + "'0', '0', 'ADMIN', '1234', DATETIME('now','localtime')" +")";

    public static final String INS_CUSTOMER =
        "INSERT INTO " + TBL_CUSTOMERS +
        "(" + COL_CARD_ID +","+ COL_CARD_NUMBER +","+ COL_NAME+","+
        COL_CARD_PW + ","+ COL_REGDATE +  ")" +
        "VALUES";

    public static final String DEL_CUSTOMERS_WHERE_CARD_NUMBER =
        "DELETE FROM " + TBL_CUSTOMERS +
        " WHERE " + COL_CARD_NUMBER + "=" ;

    public static final String SEL_ALL_CUSTOMER =
        "SELECT " + COL_CARD_ID +","+ COL_CARD_NUMBER +","+ COL_NAME+","+
                    COL_CARD_PW +","+ COL_REGDATE +
        " FROM " + TBL_CUSTOMERS;


   public static final String SEL_WHERE_CARD_NUMBER =
       "SELECT " + COL_CARD_ID +","+ COL_CARD_NUMBER +","+ COL_NAME+","+
                   COL_CARD_PW +","+ COL_REGDATE +
       " FROM " + TBL_CUSTOMERS +
       " WHERE " + COL_CARD_NUMBER + "=" + "";

   public static final String SEL_WHERE_CARD_ID =
       "SELECT " + COL_CARD_ID +","+ COL_CARD_NUMBER +","+ COL_NAME+","+
                    COL_CARD_PW + ","+ COL_REGDATE +
       " FROM " + TBL_CUSTOMERS +
       " WHERE " + COL_CARD_ID + "=" + "";

   //-------------------deals---------------------------------------

    public static final String SEL_ALL_DEAL =
            "SELECT " + COL_NUM +","+ COL_CARD_NUM +","+ COL_DEAL_TYPE+","+
            COL_BALANCE +","+ COL_AMOUNT +","+ COL_CONTENT + ","+ COL_REGDATE +
            " FROM " + TBL_DEALS;

    public static final String SEL_WHERE_DEAL =
            "SELECT " + COL_NUM +","+ COL_CARD_NUM +","+ COL_DEAL_TYPE+","+
            COL_BALANCE +","+ COL_AMOUNT +","+ COL_CONTENT + ","+ COL_REGDATE +
            " FROM " + TBL_DEALS +
            " WHERE " + COL_CARD_NUM + "=" + "";

    public static final String INS_DEAL =
            "INSERT INTO " + TBL_DEALS + "(" +
            COL_CARD_NUM + "," + COL_DEAL_TYPE + "," + COL_BALANCE + "," +
            COL_AMOUNT + "," + COL_CONTENT + ","+ COL_REGDATE + ") " +
            " VALUES" ;

    public static final String DEL_DEALS_WHERE_CARD_NUM =
            "DELETE FROM " + TBL_DEALS +
            " WHERE " + COL_CARD_NUM + "=" ;
}
