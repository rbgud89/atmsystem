package DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import model.Customer;

public class ATMDBHelper extends SQLiteOpenHelper{

    private final String TAG = "ATMDB";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "ATM.db";

    public ATMDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.i(TAG,"DBAtm()");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG,"onCreate()");
        db.execSQL(ContractDB.CREATE_TBL_CUSTOMERS);
        db.execSQL(ContractDB.CREATE_TBL_DEALS);
        insert_admin(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG,"onUpgrade()");
    }

    public void insert_admin(SQLiteDatabase db) {
        Log.i(TAG, "insert_admin()");

        db.execSQL(ContractDB.INS_ADMIN);
    }


}
